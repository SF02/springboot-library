package citi.com.training.library.Model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import citi.com.training.library.model.Cd;

public class CdTests {

    Cd soulControl = new Cd("Soul Control", "2020", "Electronic", "Jessie Ware", 12);

    @Test
    public void testCdItemName() {
        Cd cd = new Cd();
        cd.setItemName("Platypus");
        assertEquals(cd.getItemName(), "Platypus");
    }

    @Test
    public void testCdDate() {
        Cd cd = new Cd();
        cd.setDate("2020");
        assertEquals(cd.getDate(), "2020");
    }

    @Test
    public void testCdGenre() {
        Cd cd = new Cd();
        cd.setGenre("Electronic");
        assertEquals(cd.getGenre(), "Electronic");
    }

    @Test
    public void testCdArtist() {
        Cd cd = new Cd();
        cd.setArtist("Jessie Ware");
        assertEquals(cd.getArtist(), "Jessie Ware");
    }

    @Test
    public void testCdTrackNumber() {
        Cd cd = new Cd();
        cd.setTrackNumber(12);
        assertEquals(cd.getTrackNumber(), 12);
    }
}
