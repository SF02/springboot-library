package citi.com.training.library.Model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import citi.com.training.library.model.Book;

public class BookTests {

    Book mobyDick = new Book("Moby Dick", "1851", "Adventure", "Herman Melville", 585, "1503280780");

    @Test
    public void testBookItemName() {
        Book book = new Book();
        book.setItemName("Platypus");
        assertEquals(book.getItemName(), "Platypus");
    }

    @Test
    public void testBookDate() {
        Book book = new Book();
        book.setDate("1851");
        assertEquals(book.getDate(), "1851");
    }

    @Test
    public void testBookGenre() {
        Book book = new Book();
        book.setGenre("Horror");
        assertEquals(book.getGenre(), "Horror");
    }

    @Test
    public void testBookAuthor() {
        Book book = new Book();
        book.setAuthor("Henry Melville");
        assertEquals(book.getAuthor(), "Henry Melville");
    }

    @Test
    public void testBookNoPages() {
        Book book = new Book();
        book.setNoPages(1254);
        assertEquals(book.getNoPages(), 1254);
    }

    @Test
    public void testCDISBN() {
        Book book = new Book();
        book.setISBN("1234556666");
        assertEquals(book.getISBN(), "1234556666");
    }
}
