package citi.com.training.library.Model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import citi.com.training.library.model.Periodical;

public class PeriodicalTests {

    Periodical financialTimes = new Periodical("Financial Times", "1st April 2020", "Finance", "Times", 23, 14, 56);

    @Test
    public void testPeriodicalItemName() {
        Periodical periodical = new Periodical();
        periodical.setItemName("Financial Times");
        assertEquals(periodical.getItemName(), "Financial Times");
    }

    @Test
    public void testPeriodicalDate() {
        Periodical periodical = new Periodical();
        periodical.setDate("1st April 2020");
        assertEquals(periodical.getDate(), "1st April 2020");
    }

    @Test
    public void testPeriodicalGenre() {
        Periodical periodical = new Periodical();
        periodical.setGenre("Finance");
        assertEquals(periodical.getGenre(), "Finance");
    }

    @Test
    public void testPeriodicalPublisher() {
        Periodical periodical = new Periodical();
        periodical.setPublisher("Times");
        assertEquals(periodical.getPublisher(), "Times");
    }

    @Test
    public void testPeriodicalEdition() {
        Periodical periodical = new Periodical();
        periodical.setEdition(23);
        assertEquals(periodical.getEdition(), 23);
    }

    @Test
    public void testPeriodicalVolume() {
        Periodical periodical = new Periodical();
        periodical.setVolume(14);
        assertEquals(periodical.getVolume(), 14);
    }

    @Test
    public void testPeriodicalNoPages() {
        Periodical periodical = new Periodical();
        periodical.setNoPages(56);
        assertEquals(periodical.getNoPages(), 56);
    }
}
