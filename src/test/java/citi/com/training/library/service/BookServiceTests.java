package citi.com.training.library.service;

import citi.com.training.library.model.Book;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Class for unit tests of BookService class
 */
@SpringBootTest
public class BookServiceTests {
    
    @Autowired
    private BookService bookService;

    /**
     * Testing that there are items in the mongo book repository
     */
    @Test
    public void testFindAll() {
        assert(bookService.findAll().size() > 0);
    }

    /**
     * Testing that book is saved in mongo repository
     */
    @Test
    public void testBookSave() {
        Book mobyDick = new Book("Moby Dick", "1851", "Adventure", "Herman Melville", 585, "1503280780");
        bookService.save(mobyDick);
    }

    /**
     * Testing that all books removed from mongo repository
     */
    @Test
    public void testBookRemoveAll() {
        bookService.removeAll();
        assert(bookService.findAll().size() == 0);
    }
}
