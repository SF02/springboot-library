package citi.com.training.library.service;

import citi.com.training.library.model.Cd;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Class for unit tests of CdService class
 */
@SpringBootTest
public class CdServiceTests {
    
    @Autowired
    private CdService cdService;

    @Test
    public void testCdSave() {
        Cd soulControl = new Cd("Soul Control", "2020", "Electronic", "Jessie Ware", 12);

        cdService.save(soulControl);
    }


    @Test
    public void testFindAll() {
        assert(cdService.findAll().size() > 0);
    }
}
