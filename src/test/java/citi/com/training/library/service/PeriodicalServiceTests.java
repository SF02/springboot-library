package citi.com.training.library.service;

import citi.com.training.library.model.Periodical;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Class for unit tests of PeriodicalService class
 */
@SpringBootTest
public class PeriodicalServiceTests {
    
    @Autowired
    private PeriodicalService periodicalService;

    @Test
    public void testPeriodicalSave() {
        Periodical financialTimes = new Periodical("Financial Times", "1st April 2020", "Finance", "Times", 23, 14, 56);
        periodicalService.save(financialTimes);
    }

    @Test
    public void testFindAll() {
        assert(periodicalService.findAll().size() > 0);
    }
}
