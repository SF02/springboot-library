package citi.com.training.library.service;

import citi.com.training.library.dao.BookMongoRepo;
import citi.com.training.library.model.Book;

import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

/**
 * Class for unit tests of BookService class with mock objects
 */
@SpringBootTest
public class BookServiceTestsMock {
    
    @Autowired
    private BookService bookService;

    @MockBean
    private BookMongoRepo mockBookRepo;

    private static final Logger LOG = LoggerFactory.getLogger(BookServiceTests.class);

    @Test
    public void testFindAll() {
        assert(bookService.findAll().size() > 0);
    }

    @Test
    public void testBookSave() {
        Book mobyDick = new Book("Moby Dick", "1851", "Adventure", "Herman Melville", 585, "1503280780");
        LOG.debug("This is a message");
        when(mockBookRepo.save(mobyDick)).thenReturn(mobyDick);
        bookService.save(mobyDick);
    }

    @Test
    public void testBookRemoveAll() {
        Book mobyDick = new Book("Moby Dick", "1851", "Adventure", "Herman Melville", 585, "1503280780");
        bookService.save(mobyDick);
        bookService.removeAll();
        assert(bookService.findAll().size() == 0);
    }
}
