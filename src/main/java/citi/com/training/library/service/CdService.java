package citi.com.training.library.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import citi.com.training.library.dao.CdMongoRepo;
import citi.com.training.library.model.Cd;

/**
 * Class that represents interaction of API with mongo repository for Cd objects
 */
@Component
public class CdService {
    
    @Autowired
    private CdMongoRepo mongoRepo;

    /**
     * Method returns all documents in CD repository
     * @return - JSON object containing all CD documents
     */
    public List<Cd> findAll() {
        return mongoRepo.findAll();
    }

    /**
     * Save CD into CD repository 
     * @param cd - CD object
     * @return - JSON object containing the CD
     */
    public Cd save(Cd cd) {
        return mongoRepo.save(cd);
    }
}
