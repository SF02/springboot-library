package citi.com.training.library.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import citi.com.training.library.dao.BookMongoRepo;
import citi.com.training.library.model.Book;

/**
 * Class that represents interaction of API with mongo repository for Book objects
 */
@Component
public class BookService {

    @Autowired
    private BookMongoRepo mongoRepo;

    /**
     * Method returns all documents in book repository
     * @return - JSON object containing all book documents
     */
    public List<Book> findAll() {
        return mongoRepo.findAll();
    }

    /**
     * Save book into book repository
     * @param book - book object
     * @return - JSON object containing the book
     */
    public Book save(Book book) {
        return mongoRepo.save(book);
    }

    /**
     * Remove all books from book repository
     */
    public void removeAll() {
        mongoRepo.deleteAll();
    }
}
