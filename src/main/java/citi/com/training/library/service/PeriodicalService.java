package citi.com.training.library.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import citi.com.training.library.dao.PeriodicalMongoRepo;
import citi.com.training.library.model.Periodical;

/**
 * Class that represents interaction of API with mongo repository for Periodical objects
 */
@Component
public class PeriodicalService {
    
    @Autowired
    private PeriodicalMongoRepo mongoRepo;

    /**
     * Method returns all documents in Periodical repository
     * @return - JSON object containing all Periodical documents
     */
    public List<Periodical> findAll() {
        return mongoRepo.findAll();
    }
    
    /**
     * Save Peridoical into Periodical repository 
     * @param cd - Periodical object
     * @return - JSON object containing the Periodical
     */
    public Periodical save(Periodical periodical) {
        return mongoRepo.save(periodical);
    }
}
