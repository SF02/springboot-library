package citi.com.training.library.model;

/**
 * Class that represents a CD
 */
public class Cd extends Item{
    private String artist;
    private int trackNumber;

    /**
     * Zero arg constructor
     */
    public Cd() {
    }

    /**
     * Constructor with args
     * @param itemName - item name
     * @param date - date of release
     * @param genre - genre of music
     * @param artist - artist name
     * @param trackNumber - number of tracks
     */
    public Cd(String itemName, String date, String genre, String artist, int trackNumber) {
        super(itemName, date, genre);
        this.artist = artist;
        this.trackNumber = trackNumber;
    }

    /**
     * Getter for artist name
     * @return
     */
    public String getArtist() {
        return artist;
    }

    /**
     * Setter for artist name
     */
    public void setArtist(String artist) {
        this.artist = artist;
    }

    /**
     * Getter for number of tracks
     * @return
     */
    public int getTrackNumber() {
        return trackNumber;
    }

    /**
     * Setter for number of tracks
     * @param trackNumber
     */
    public void setTrackNumber(int trackNumber) {
        this.trackNumber = trackNumber;
    }

    /**
     * toString() method for CD
     */
    @Override
    public String toString() {
        return "CD [artist=" + artist + ", trackNumber=" + trackNumber + "]";
    }    
}
