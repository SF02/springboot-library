package citi.com.training.library.model;

/**
 * Class that represents an abstract item
 */
public abstract class Item {
    private String itemName;
    private String date;
    private String genre;

    /**
     * Zero args constructor
     */
    public Item() {
    }
    
    /**
     * Constructor with args
     * @param itemName - item name
     * @param date - date of release
     * @param genre - genre of item
     */
    public Item(String itemName, String date, String genre) {
        this.itemName = itemName;
        this.date = date;
        this.genre = genre;
    }

    /**
     * Getter for item name
     * @return
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * Setter for item name
     * @param itemName
     */
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    /**
     * Getter for date of release
     * @return
     */
    public String getDate() {
        return date;
    }

    /**
     * Setter for date of release
     * @param date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Getter for genre
     * @return
     */
    public String getGenre() {
        return genre;
    }

    /**
     * Setter for genre
     * @param genre
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    /**
     * toString() method for item
     */
    @Override
    public String toString() {
        return "Item [date=" + date + ", genre=" + genre + ", itemName=" + itemName + "]";
    }
}
