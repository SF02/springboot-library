package citi.com.training.library.model;

/**
 * Class that represents a periodical
 */
public class Periodical extends Item{

    private String publisher;
    private int edition;
    private int volume;
    private int noPages;

    /**
     * Zero arg constructor
     */
    public Periodical() {
    }

    /**
     * Constructor with args
     * @param itemName - item name
     * @param date - date of release
     * @param genre - genre of periodical
     * @param publisher - name of periodical
     * @param edition - edition of periodical
     * @param volume - volume of periodical
     * @param noPages - number of pages
     */
    public Periodical(String itemName, String date, String genre, String publisher, int edition, int volume,
            int noPages) {
        super(itemName, date, genre);
        this.publisher = publisher;
        this.edition = edition;
        this.volume = volume;
        this.noPages = noPages;
    }

    /**
     * Getter for publisher name
     * @return
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * Setter for publisher name
     * @param publisher
     */
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    /**
     * Getter for edition
     * @return
     */
    public int getEdition() {
        return edition;
    }

    /**
     * Setter for edition
     * @param edition
     */
    public void setEdition(int edition) {
        this.edition = edition;
    }

    /**
     * Getter for volume
     * @return
     */
    public int getVolume() {
        return volume;
    }

    /**
     * Setter for volume
     * @param volume
     */
    public void setVolume(int volume) {
        this.volume = volume;
    }

    /**
     * Getter for number of pages
     * @return
     */
    public int getNoPages() {
        return noPages;
    }

    /**
     * Setter for number of pages
     * @param noPages
     */
    public void setNoPages(int noPages) {
        this.noPages = noPages;
    }

    /**
     * toString() method
     */
    @Override
    public String toString() {
        return "Periodical [edition=" + edition + ", noPages=" + noPages + ", publisher=" + publisher + ", volume="
                + volume + "]";
    }

    
}
