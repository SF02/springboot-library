package citi.com.training.library.model;

/**
 * Class that represents a book
 */
public class Book extends Item {
    private String author;
    private int noPages;
    private String ISBN;

    /**
     * Zero argument constructor
     */
    public Book() {
    }

    /**
     * Constructor with args
     * @param itemName - name of item
     * @param date - date of release
     * @param genre - genre of book
     * @param author - author
     * @param noPages - number of pages
     * @param iSBN - ISBN number
     */
    public Book(String itemName, String date, String genre, String author, int noPages, String iSBN) {
        super(itemName, date, genre);
        this.author = author;
        this.noPages = noPages;
        ISBN = iSBN;
    }

    /**
     * Getter for author
     * @return
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Setter for author
     * @param author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Getter for number of pages
     * @return
     */
    public int getNoPages() {
        return noPages;
    }

    /**
     * Setter for number of pages
     * @param noPages
     */
    public void setNoPages(int noPages) {
        this.noPages = noPages;
    }

    /**
     * Getter for ISBN
     * @return
     */
    public String getISBN() {
        return ISBN;
    }

    /**
     * Setter for ISBN
     */
    public void setISBN(String iSBN) {
        ISBN = iSBN;
    }

    /**
     * toString() method for book
     */
    @Override
    public String toString() {
        return "Book [ISBN=" + ISBN + ", author=" + author + ", noPages=" + noPages + "]";
    }
    
}
