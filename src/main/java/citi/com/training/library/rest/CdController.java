package citi.com.training.library.rest;

import java.util.List;

import citi.com.training.library.service.CdService;
import citi.com.training.library.model.Cd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/v1/library/cd")
public class CdController {
    
    private static final Logger LOG = LoggerFactory.getLogger(CdController.class);

    @Autowired
    private CdService cdService;

    @RequestMapping(method=RequestMethod.GET) 
    public List<Cd> findAll() {
        LOG.debug("findAll() request received");
        return cdService.findAll();
    }

    @RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<Cd> save(@RequestBody Cd cd) {
        LOG.debug("save cd request received");
        return new ResponseEntity<Cd>(cdService.save(cd), HttpStatus.CREATED);

    }
}
