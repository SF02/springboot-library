package citi.com.training.library.rest;

import java.util.List;

import citi.com.training.library.service.PeriodicalService;
import citi.com.training.library.model.Periodical;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/v1/library/periodical")
public class PeriodicalController {
    
    private static final Logger LOG = LoggerFactory.getLogger(PeriodicalController.class);

    @Autowired
    private PeriodicalService periodicalService;

    @RequestMapping(method=RequestMethod.GET) 
    public List<Periodical> findAll() {
        LOG.debug("findAll() request received");
        return periodicalService.findAll();
    }

    @RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<Periodical> save(@RequestBody Periodical periodical) {
        LOG.debug("save periodical request received");
        return new ResponseEntity<Periodical>(periodicalService.save(periodical), HttpStatus.CREATED);

    }
}
