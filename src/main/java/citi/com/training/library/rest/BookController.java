package citi.com.training.library.rest;

import java.util.List;

import citi.com.training.library.service.BookService;
import citi.com.training.library.model.Book;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/v1/library/book")
public class BookController {
    
    private static final Logger LOG = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private BookService bookService;

    @RequestMapping(method=RequestMethod.GET) 
    public List<Book> findAll() {
        LOG.debug("findAll() request received");
        return bookService.findAll();
    }

    @RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<Book> save(@RequestBody Book book) {
        LOG.debug("save book request received");
        return new ResponseEntity<Book>(bookService.save(book), HttpStatus.CREATED);

    }
}
