package citi.com.training.library.dao;

import citi.com.training.library.model.Book;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface BookMongoRepo extends MongoRepository<Book, String> {
    
}
