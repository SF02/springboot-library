package citi.com.training.library.dao;

import citi.com.training.library.model.Cd;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface CdMongoRepo extends MongoRepository<Cd, String> {
    
}
