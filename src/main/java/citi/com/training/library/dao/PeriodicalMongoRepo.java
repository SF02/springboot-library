package citi.com.training.library.dao;

import citi.com.training.library.model.Periodical;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface PeriodicalMongoRepo extends MongoRepository<Periodical, String> {
    
}
